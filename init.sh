#!/bin/bash

# Nom de votre application Node.js
APP_NAME="votre-application-node"

start() {
  echo "Démarrage de l'application $APP_NAME"
  pm2 start $APP_NAME.js
}

stop() {
  echo "Arrêt de l'application $APP_NAME"
  pm2 stop $APP_NAME
}

restart() {
  echo "Redémarrage de l'application $APP_NAME"
  pm2 restart $APP_NAME
}

status() {
  pm2 list
}

case "$1" in
  start)
    start
    ;;
  stop)
    stop
    ;;
  restart)
    restart
    ;;
  status)
    status
    ;;
  *)
    echo "Utilisation : $0 {start|stop|restart|status}"
    exit 1
esac

exit 0



#!/bin/bash

# Le chemin vers le fichier principal de votre application Node.js
APP_PATH="/chemin/vers/votre/application/app.js"
# Le port sur lequel votre application Node.js écoute
APP_PORT="3000"

start() {
  if [ -f "$APP_PATH" ]; then
    # Vérifier si l'application est déjà en cours d'exécution
    if [ -z "$(lsof -i :$APP_PORT)" ]; then
      echo "Démarrage de l'application Node.js"
      node "$APP_PATH" &
    else
      echo "L'application est déjà en cours d'exécution sur le port $APP_PORT."
    fi
  else
    echo "Fichier d'application introuvable : $APP_PATH"
  fi
}

stop() {
  echo "Arrêt de l'application Node.js"
  pkill -f "node $APP_PATH"
}

restart() {
  stop
  start
}

status() {
  if [ -n "$(lsof -i :$APP_PORT)" ]; then
    echo "L'application Node.js est en cours d'exécution sur le port $APP_PORT."
  else
    echo "L'application Node.js n'est pas en cours d'exécution."
  fi
}

case "$1" in
  start)
    start
    ;;
  stop)
    stop
    ;;
  restart)
    restart
    ;;
  status)
    status
    ;;
  *)
    echo "Utilisation : $0 {start|stop|restart|status}"
    exit 1
esac

exit 0




#!/bin/bash

NODE_CMD="node"  # Commande Node.js
NODE_FILE="server.js"  # Fichier Node.js à exécuter
PID_FILE="server.pid"  # Fichier pour stocker le PID du processus

start_server() {
  if [ -f "$PID_FILE" ]; then
    echo "Le serveur est déjà en cours d'exécution."
  else
    $NODE_CMD $NODE_FILE &
    echo $! > $PID_FILE
    echo "Serveur Node.js démarré avec PID $!"
  fi
}

stop_server() {
  if [ -f "$PID_FILE" ]; then
    PID=$(cat "$PID_FILE")
    kill $PID
    rm "$PID_FILE"
    echo "Serveur Node.js arrêté (PID $PID)."
  else
    echo "Le serveur n'est pas en cours d'exécution."
  fi
}

restart_server() {
  stop_server
  start_server
}

status_server() {
  if [ -f "$PID_FILE" ]; then
    PID=$(cat "$PID_FILE")
    echo "Le serveur Node.js est en cours d'exécution (PID $PID)."
  else
    echo "Le serveur n'est pas en cours d'exécution."
  fi
}

case "$1" in
  start)
    start_server
    ;;
  stop)
    stop_server
    ;;
  restart)
    restart_server
    ;;
  status)
    status_server
    ;;
  *)
    echo "Utilisation : $0 {start|stop|restart|status}"
    exit 1
    ;;
esac
